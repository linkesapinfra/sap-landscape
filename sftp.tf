module "sftp_server" {
    source = "git@bitbucket.org:linkesapinfra/sftp-service.git"

    name            = "sftp-for-sap"
    vpc_id          = module.vpc.vpc_id
    project_code    = var.project_code
    subnet_ids      = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
    tags            = var.global_tags

    ftp_users = {
      user1 = {
        username                  = "user"
        uid                       = 1010
        gid                       = 1010
        home_directory            = format("/%s/interfaces", aws_efs_file_system.efs.id)
        ssh_key                   = "publicKey1"
      }
    }
}
