# SAP Landscape #

This is an example of a complete landscape for a SAP infrastructure. This example include:
  - SAP appl Instance
  - HANA Instance
  - WebDispatcher
  - SAP router
  - Security Groups
  - IAM profiles
  - VPC and subnets.


## Usage

For use this code is important to understand the modules that we will use.

The most easy way is to clone this example and check and change the code for the elements. Pay atention to the files structure, you can see that each kind of element is described in a file with a friendly name

You can see "two" kind of elements:
  - Based on modules
  - Based on resources

## Based on modules
These are the most complex elements, and so we have the modules for to simplify the code. Please check the README.md file in each module if you need to know the parameters for the module
In these casse we only must call the module with the correct parameters

## Based on resources
We use the resources for the simple elements, in these cases is not necessary to deploy a specific module.
Usually we use this kind of descriptions for common elements and in most cases we only need to "copy" the code.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | = 3.40 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | = 3.40 |
| <a name="provider_local"></a> [local](#provider\_local) | n/a |
| <a name="provider_tls"></a> [tls](#provider\_tls) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_app_hana"></a> [app\_hana](#module\_app\_hana) | git@bitbucket.org:linkesapinfra/ec2-sap-hana.git | n/a |
| <a name="module_app_instance"></a> [app\_instance](#module\_app\_instance) | git@bitbucket.org:linkesapinfra/ec2-sap-appl.git | n/a |
| <a name="module_app_saprouter"></a> [app\_saprouter](#module\_app\_saprouter) | git@bitbucket.org:linkesapinfra/ec2-sap-appl.git | n/a |
| <a name="module_app_web_dispatcher"></a> [app\_web\_dispatcher](#module\_app\_web\_dispatcher) | git@bitbucket.org:linkesapinfra/ec2-sap-appl.git | n/a |
| <a name="module_daaadm-pass"></a> [daaadm-pass](#module\_daaadm-pass) | git@bitbucket.org:linkesapinfra/user-pass.git | n/a |
| <a name="module_iam-profile"></a> [iam-profile](#module\_iam-profile) | git@bitbucket.org:linkesapinfra/iam-profile-sap.git | n/a |
| <a name="module_prefix"></a> [prefix](#module\_prefix) | git@bitbucket.org:linkesapinfra/naming-sap.git | n/a |
| <a name="module_sapadm-pass"></a> [sapadm-pass](#module\_sapadm-pass) | git@bitbucket.org:linkesapinfra/user-pass.git | n/a |
| <a name="module_sftp_server"></a> [sftp\_server](#module\_sftp\_server) | git@bitbucket.org:linkesapinfra/sftp-service.git | n/a |
| <a name="module_sg-admin"></a> [sg-admin](#module\_sg-admin) | git@bitbucket.org:linkesapinfra/sg-common-sap.git | n/a |
| <a name="module_sg-appl"></a> [sg-appl](#module\_sg-appl) | git@bitbucket.org:linkesapinfra/sg-common-sap.git | n/a |
| <a name="module_sg-ascs"></a> [sg-ascs](#module\_sg-ascs) | git@bitbucket.org:linkesapinfra/sg-common-sap.git | n/a |
| <a name="module_sg-efs"></a> [sg-efs](#module\_sg-efs) | git@bitbucket.org:linkesapinfra/sg-minimal-sap.git | n/a |
| <a name="module_sg-hana"></a> [sg-hana](#module\_sg-hana) | git@bitbucket.org:linkesapinfra/sg-common-sap.git | n/a |
| <a name="module_sg-saprouter"></a> [sg-saprouter](#module\_sg-saprouter) | git@bitbucket.org:linkesapinfra/sg-minimal-sap.git | n/a |
| <a name="module_sg-wd"></a> [sg-wd](#module\_sg-wd) | git@bitbucket.org:linkesapinfra/sg-common-sap.git | n/a |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_efs_file_system.efs](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/resources/efs_file_system) | resource |
| [aws_efs_mount_target.efs_mount_point](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/resources/efs_mount_target) | resource |
| [aws_key_pair.auth](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/resources/key_pair) | resource |
| [aws_s3_bucket.backups](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/resources/s3_bucket) | resource |
| [local_file.key_pem](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [tls_private_key.key](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/data-sources/caller_identity) | data source |
| [aws_dynamodb_table.lock](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/data-sources/dynamodb_table) | data source |
| [aws_kms_key.ebs](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/data-sources/kms_key) | data source |
| [aws_kms_key.efs](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/data-sources/kms_key) | data source |
| [aws_kms_key.s3](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/data-sources/kms_key) | data source |
| [aws_s3_bucket.bck_ansible](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/data-sources/s3_bucket) | data source |
| [aws_s3_bucket.bck_tfstate](https://registry.terraform.io/providers/hashicorp/aws/3.40/docs/data-sources/s3_bucket) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami-id"></a> [ami-id](#input\_ami-id) | AMI Id | `string` | `"ami-0e39a11a4694e879d"` | no |
| <a name="input_efs_name"></a> [efs\_name](#input\_efs\_name) | n/a | `string` | `"my-efs"` | no |
| <a name="input_global_tags"></a> [global\_tags](#input\_global\_tags) | n/a | `map(string)` | <pre>{<br>  "Deployer": "LinkeIT",<br>  "Owner": "salvador.alavedra@syntax.com",<br>  "Project": "sap-ansible"<br>}</pre> | no |
| <a name="input_key_pair"></a> [key\_pair](#input\_key\_pair) | n/a | `string` | `"test-sap"` | no |
| <a name="input_project_code"></a> [project\_code](#input\_project\_code) | Project code | `string` | `"lnk-demo"` | no |
| <a name="input_region"></a> [region](#input\_region) | n/a | `string` | `"eu-west-1"` | no |
| <a name="input_vpc_name"></a> [vpc\_name](#input\_vpc\_name) | n/a | `string` | `"my-vpc"` | no |

## Outputs

No outputs.

## Authors

Module is maintained by [Salvador Alavedra](https://teams.microsoft.com/l/chat/0/0?users=salvador.alavedra@syntax.com) and [Javier Sahagun](https://teams.microsoft.com/l/chat/0/0?users=javier.sahagun@syntax.com).

## FAQ

#### Can I request new features?
See [Contributions](#Feedback)  section. Don't forget to check the [Change Log](CHANGELOG.md) before.
#### This module helps enforce best practices?
AWS published [SAP Lens](https://docs.aws.amazon.com/wellarchitected/latest/sap-lens/sap-lens.pdf) and this terraform module was created to help with some of the points listed there.
#### Rebuild from scratch is needed?
Existing AWS resources in your current AWS account(s) can be reused without downtime by this module via the ```terraform import
``` command. Be careful in your production environments, some settings can overwrite the existing ones.

## Feedback

Contributions are always welcome! Please read the [contribution guidelines](CONTRIBUTING.md) first

## Reporting a Vulnerability

If you find a security vulnerability affecting any of the supported roles, please email to authors following the [guidelines](CONTRIBUTING.md).

After receiving the initial report, we will endeavor to keep you informed of the progress towards a fix and full announcement.
We may ask you for additional information. You are also welcome to propose a patch or solution.

Report security bugs in third-party software to the person or team maintaining it.

## License

[Apache 2 Licensed](http://www.apache.org/licenses/LICENSE-2.0). See [LICENSE](LICENSE) for full details.

## Appendix

Any additional information goes [here](https://www.syntax.com)
<!-- END OF PRE-COMMIT-TERRAFORM DOCS -->