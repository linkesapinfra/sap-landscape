variable "key_pair" {
  default = "test-sap"
}
variable "vpc_name" {
  default = "my-vpc"
}
variable "efs_name" {
  default = "my-efs"
}
variable "project_code" {
  type = string
  default = "lnk-demo"
  description = "Project code"
}
variable "ami-id" {
  type = string
# https://launchpad.support.sap.com/#/notes/1618572
  default = "ami-0e39a11a4694e879d" // Red Hat Enterprise Linux for SAP with HA and US 7.6v1 (May 03. 2019) 64-bit x86
  description = "AMI Id"
}
variable "global_tags" {
  type = map(string)
  default = {
    "Owner"    = "salvador.alavedra@syntax.com"
    "Deployer" = "LinkeIT"
    "Project"  = "sap-ansible"
  }
}
variable "region" {
  type        = string
  default     = "eu-west-1"
}
