data "aws_caller_identity" "current" {}

data "aws_s3_bucket" "bck_ansible" {
  bucket = format("ansible-ssm-%s", data.aws_caller_identity.current.account_id)
}

data "aws_s3_bucket" "bck_tfstate" {
  bucket = format("tfstate-%s", data.aws_caller_identity.current.account_id)
}

data "aws_dynamodb_table" "lock" {
  name = format("lock-table-%s", data.aws_caller_identity.current.account_id)
}

data "aws_kms_key" "ebs" {
  key_id = "alias/CMK-KMS-EBS"
}

data "aws_kms_key" "efs" {
  key_id = "alias/CMK-KMS-EFS"
}

data "aws_kms_key" "s3" {
  key_id = "alias/CMK-KMS-S3"
}
