resource "aws_s3_bucket" "backups" {
  bucket      = format("backup-%s", data.aws_caller_identity.current.account_id)
  acl         = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = data.aws_kms_key.s3.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
  tags = merge(
    var.global_tags, tomap({ "Name" = format("%s-Backup", module.prefix.bucket)})
    )
}
