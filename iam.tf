module "iam-profile" {
  source            = "git@bitbucket.org:linkesapinfra/iam-profile-sap.git"
  name              = "sap-prf-default"
  is_ha             = false
  kms_list          = [data.aws_kms_key.ebs.arn, data.aws_kms_key.efs.arn, data.aws_kms_key.s3.arn]
  routetables_list  = module.vpc.private_route_table_ids
  global_tags       = var.global_tags
}
