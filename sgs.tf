module "sg-admin" {
  source            = "git@bitbucket.org:linkesapinfra/sg-common-sap.git"
  vpc_id            = module.vpc.vpc_id
  sgs_type          = "admin"
  project_code      = var.project_code
  cidr_ingress_rule = ["172.1.0.0/16", "192.168.0.0/16"]
  tags              = var.global_tags
}

module "sg-ascs" {
  source            = "git@bitbucket.org:linkesapinfra/sg-common-sap.git"
  vpc_id            = module.vpc.vpc_id
  sgs_type          = "abap-ascs"
  ascs_sn           = "11"
  project_code      = var.project_code
  cidr_ingress_rule = ["172.1.0.0/16", "192.168.0.0/16"]
  tags              = var.global_tags
}

module "sg-appl" {
  source            = "git@bitbucket.org:linkesapinfra/sg-common-sap.git"
  vpc_id            = module.vpc.vpc_id
  sgs_type          = "abap-appl"
  ascs_sn           = "21"
  project_code      = var.project_code
  cidr_ingress_rule = ["172.1.0.0/16", "192.168.0.0/16"]
  tags              = var.global_tags
}

module "sg-hana" {
  source            = "git@bitbucket.org:linkesapinfra/sg-common-sap.git"
  vpc_id            = module.vpc.vpc_id
  sgs_type          = "hana"
  ascs_sn           = "01"
  project_code      = var.project_code
  cidr_ingress_rule = ["172.1.0.0/16", "192.168.0.0/16"]
  tags              = var.global_tags
}

module "sg-wd" {
  source            = "git@bitbucket.org:linkesapinfra/sg-common-sap.git"
  vpc_id            = module.vpc.vpc_id
  sgs_type          = "webdispatcher"
  ascs_sn           = "31"
  project_code      = var.project_code
  cidr_ingress_rule = ["93.176.139.58/32"]
  tags              = var.global_tags
}

module "sg-saprouter" {
  source          = "git@bitbucket.org:linkesapinfra/sg-minimal-sap.git"
  vpc_id          = module.vpc.vpc_id
  tags            = var.global_tags
  name            = "saprouter"
  project_code    = var.project_code
  descrp          = "Security Group for SApRouter Instances"
  sg_rules_cidr = {
    saprouter = {
      description = "SAProuter Port"
      from_port   = "3299"
      to_port     = "3299"
      cidr_list   = ["194.39.131.34/32"]
    }
  }
}
