# provider "aws" {
#    #region = var.region
#    region = "eu-west-1"
# }

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.40"
    }
  }
  backend "s3" {
   encrypt        = true
   #bucket         = data.aws_s3_bucket.bck_tfstate
   bucket         = "tfstate-192944331906"
   #region         = var.region
   region         = "eu-west-1"
   #key            = format("%s/terraform.tfstate", var.project_code)
   key            = "lnk-demo/terraform.tfstate"
   #dynamodb_table = data.aws_dynamodb_table.lock
   dynamodb_table = "lock-table-192944331906"
  }
}
