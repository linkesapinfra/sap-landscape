## Authors

Module is maintained by [Salvador Alavedra](https://teams.microsoft.com/l/chat/0/0?users=salvador.alavedra@syntax.com) and [Javier Sahagun](https://teams.microsoft.com/l/chat/0/0?users=javier.sahagun@syntax.com).

## FAQ

#### Can I request new features?
See [Contributions](#Feedback)  section. Don't forget to check the [Change Log](CHANGELOG.md) before.
#### This module helps enforce best practices?
AWS published [SAP Lens](https://docs.aws.amazon.com/wellarchitected/latest/sap-lens/sap-lens.pdf) and this terraform module was created to help with some of the points listed there.
#### Rebuild from scratch is needed?
Existing AWS resources in your current AWS account(s) can be reused without downtime by this module via the ```terraform import``` command. Be careful in your production environments, some settings can overwrite the existing ones.


## Feedback

Contributions are always welcome! Please read the [contribution guidelines](CONTRIBUTING.md) first

## Reporting a Vulnerability

If you find a security vulnerability affecting any of the supported roles, please email to authors following the [guidelines](CONTRIBUTING.md).

After receiving the initial report, we will endeavor to keep you informed of the progress towards a fix and full announcement.
We may ask you for additional information. You are also welcome to propose a patch or solution.

Report security bugs in third-party software to the person or team maintaining it.

## License

[Apache 2 Licensed](http://www.apache.org/licenses/LICENSE-2.0). See [LICENSE](LICENSE) for full details.

## Appendix

Any additional information goes [here](https://www.syntax.com)
