module "app_instance" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-appl.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile.id
  name                   = "sap-appl-01"
  instance_type          = "r5.large"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.private_subnets[0]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-ascs.abap-ascs-id, module.sg-appl.abap-appl-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "testapplinst"
      "Type"     = "appl"
      "SID"      = "LNS"
      "EFS"      = aws_efs_mount_target.efs_mount_point[0].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  # Define EBS volumes
  root_size          = "15"
  bin_volumes_count  = "1"
  bin_volumes_size   = "25"
  swap_volumes_count = "1"
  swap_volumes_size  = "20"
}

module "app_hana" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-hana.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile.id
  name                   = "sap-hana-01"
  instance_type          = "r5.4xlarge"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.private_subnets[1]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-hana.hana-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "testhanalinst"
      "Type"     = "hana"
      "SID"      = "LNH"
      "EFS"      = aws_efs_mount_target.efs_mount_point[1].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  # Define EBS volumes
  root_size            = "15"
  bin_volumes_count    = "1"
  bin_volumes_size     = "25"
  data_volumes_count   = "3"
  data_volumes_size    = "25"
  logs_volumes_count   = "2"
  logs_volumes_size    = "15"
  shared_volumes_count = "1"
  shared_volumes_size  = "30"
  backup_volumes_count = "1"
  backup_volumes_size  = "50"
}

module "app_web_dispatcher" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-appl.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile.id
  name                   = "sap-wd-01"
  instance_type          = "r5.large"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-wd.wd-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  autorecover            = true
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "testwdinst"
      "Type"     = "appl"
      "SID"      = "LNW"
      "EFS"      = aws_efs_mount_target.efs_mount_point[0].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  public_ip              = true
  # Define EBS volumes
  root_size          = "10"
  bin_volumes_count  = "1"
  bin_volumes_size   = "25"
  swap_volumes_count = "1"
  swap_volumes_size  = "20"
}

module "app_saprouter" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-appl.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile.id
  name                   = "sap-sr-01"
  instance_type          = "t3.small"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.public_subnets[1]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-saprouter.id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  autorecover            = true
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "saprouter"
      "Type"     = "appl"
      "SID"      = "LNW"
      "EFS"      = aws_efs_mount_target.efs_mount_point[0].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  public_ip              = true
  # Define EBS volumes
  root_size          = "10"
  bin_volumes_count  = "1"
  bin_volumes_size   = "10"
  swap_volumes_count = "1"
  swap_volumes_size  = "20"
}
