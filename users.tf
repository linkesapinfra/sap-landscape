module "sapadm-pass" {
  source = "git@bitbucket.org:linkesapinfra/user-pass.git"
  username = "sapadm"
  project_code = var.project_code
}

module "daaadm-pass" {
  source = "git@bitbucket.org:linkesapinfra/user-pass.git"
  username = "daaadm"
  project_code = var.project_code
}
