resource "aws_efs_file_system" "efs" {
  creation_token   = format("%s-%s", module.prefix.efs, var.efs_name)
  performance_mode = "generalPurpose"
  encrypted        = true
  kms_key_id       = data.aws_kms_key.efs.arn
  tags             = var.global_tags
}
resource "aws_efs_mount_target" "efs_mount_point" {
  count = length(module.vpc.private_subnets)
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = module.vpc.private_subnets[count.index]
  security_groups = [module.sg-efs.id]
}

module "sg-efs" {
  source       = "git@bitbucket.org:linkesapinfra/sg-minimal-sap.git"
  vpc_id       = module.vpc.vpc_id
  tags         = var.global_tags
  name         = "efs"
  project_code    = var.project_code
  sg_rules_cidr = {
    nfs = {
      description = "NFS"
      from_port   = "2049"
      to_port     = "2049"
      cidr_list   = ["172.1.0.0/16", "192.168.0.0/16"]
    }
  }
}
